# php-exercises

## 1/ HELLO WORLD ! (1)

### Instructions
The classical introductory exercise. Just say "Hello, World!".

"Hello, World!" is the traditional first program for beginning programming in a new language or environment.

The objectives are simple:

Write a function that returns the string "Hello, World!".
Run the test suite and make sure that it succeeds.
Submit your solution and check it at the website.
If everything goes well, you will be ready to fetch your first real exercise.

```php
<?php

function helloWorld()
{
    return "Good Bye, World!";
}
```

## 2/ Reverse String (1)

### Instructions
Reverse a string

For example: input: "cool" output: "looc"

```php
<?php

function reverseString(string $text): string
{
    return '';
}
```

## 3/ Robot Name (2)

### Instructions
Manage robot factory settings.

When a robot comes off the factory floor, it has no name.

The first time you turn on a robot, a random name is generated in the format of two uppercase letters followed by three digits, such as RX837 or BC811.

Every once in a while we need to reset a robot to its factory settings, which means that its name gets wiped. The next time you ask, that robot will respond with a new random name.

The names must be random: they should not follow a predictable sequence. Using random names means a risk of collisions. Your solution must ensure that every existing robot has a unique name.

```php
<?php

class Robot
{
    public function getName(): string
    {
        throw new \BadMethodCallException("Implement the getName method");
    }

    public function reset(): void
    {
        throw new \BadMethodCallException("Implement the reset method");
    }
}
```

## 4/ Mask credit card (2)

### Instructions
Create a function `maskify` to mask digits of a credit card number with `#`.

**Requirements:**

- Do not mask the first digit and the last four digits
- Do not mask non-digit chars
- Do not mask if the input is less than 6
- Return '' when input is empty

**Examples:**

- `1234-5678-9012` converts to `1###-####-9012`
- `123456789012` converts to `1#######9012`

```php
function maskify(string $cc): string
{
    throw new \BadFunctionCallException("Implement the maskify function");
}
```

## 5/ Clock (2)

### Instructions
Implement a clock that handles times without dates.

You should be able to add and subtract minutes to it.

Two clocks that represent the same time should be equal to each other.

```php
<?php

class Clock
{
    public function __toString(): string
    {
        throw new \BadMethodCallException("Implement the __toString function");
    }
}
```

## 6/ Grade School (2)

### Instructions
Given students' names along with the grade that they are in, create a roster
for the school.

In the end, you should be able to:

- Add a student's name to the roster for a grade
  - "Add Jim to grade 2."
  - "OK."
- Get a list of all students enrolled in a grade
  - "Which students are in grade 2?"
  - "We've only got Jim just now."
- Get a sorted list of all students in all grades. Grades should sort
  as 1, 2, 3, etc., and students within a grade should be sorted
  alphabetically by name.
  - "Who all is enrolled in school right now?"
  - "Let me think. We have
  Anna, Barb, and Charlie in grade 1,
  Alex, Peter, and Zoe in grade 2
  and Jim in grade 5.
  So the answer is: Anna, Barb, Charlie, Alex, Peter, Zoe and Jim"

Note that all our students only have one name.  (It's a small town, what
do you want?)

### For bonus points

Did you get the tests passing and the code clean? If you want to, these
are some additional things you could try:

- If you're working in a language with mutable data structures and your
  implementation allows outside code to mutate the school's internal DB
  directly, see if you can prevent this. Feel free to introduce additional
  tests.

Then please share your thoughts in a comment on the submission. Did this
experiment make the code better? Worse? Did you learn anything from it?

```php
<?php

class School
{
    public function add(string $name, int $grade): void
    {
        throw new \BadMethodCallException("Implement the add method");
    }

    public function grade($grade)
    {
        throw new \BadMethodCallException("Implement the grade method");
    }

    public function studentsByGradeAlphabetical(): array
    {
        throw new \BadMethodCallException("Implement the studentsByGradeAlphabetical method");
    }
}
```

## 7/ The Matrix (3)

### Instructions
Given a string representing a matrix of numbers, return the rows and columns of
that matrix.

So given a string with embedded newlines like:

```text
9 8 7
5 3 2
6 6 7
```

representing this matrix:

```text
    1  2  3
  |---------
1 | 9  8  7
2 | 5  3  2
3 | 6  6  7
```

your code should be able to spit out:

- A list of the rows, reading each row left-to-right while moving
  top-to-bottom across the rows,
- A list of the columns, reading each column top-to-bottom while moving
  from left-to-right.

The rows for our example matrix:

- 9, 8, 7
- 5, 3, 2
- 6, 6, 7

And its columns:

- 9, 5, 6
- 8, 3, 6
- 7, 2, 7

```php
<?php

class Matrix
{
    public function __construct()
    {
        throw new BadFunctionCallException("Please implement the Matrix class!");
    }
}
```

## 8/ Dungeons & Dragons Character (3)

### Instructions
For a game of [Dungeons & Dragons][DND], each player starts by generating a
character they can play with. This character has, among other things, six
abilities; strength, dexterity, constitution, intelligence, wisdom and
charisma. These six abilities have scores that are determined randomly. You
do this by rolling four 6-sided dice and record the sum of the largest three
dice. You do this six times, once for each ability.

Your character's initial hitpoints are 10 + your character's constitution
modifier. You find your character's constitution modifier by subtracting 10
from your character's constitution, divide by 2 and round down.

Write a random character generator that follows the rules above.

For example, the six throws of four dice may look like:

* 5, 3, 1, 6: You discard the 1 and sum 5 + 3 + 6 = 14, which you assign to strength.
* 3, 2, 5, 3: You discard the 2 and sum 3 + 5 + 3 = 11, which you assign to dexterity.
* 1, 1, 1, 1: You discard the 1 and sum 1 + 1 + 1 = 3, which you assign to constitution.
* 2, 1, 6, 6: You discard the 1 and sum 2 + 6 + 6 = 14, which you assign to intelligence.
* 3, 5, 3, 4: You discard the 3 and sum 5 + 3 + 4 = 12, which you assign to wisdom.
* 6, 6, 6, 6: You discard the 6 and sum 6 + 6 + 6 = 18, which you assign to charisma.

Because constitution is 3, the constitution modifier is -4 and the hitpoints are 6.

## Notes

Most programming languages feature (pseudo-)random generators, but few
programming languages are designed to roll dice. One such language is [Troll].

[DND]: https://en.wikipedia.org/wiki/Dungeons_%26_Dragons
[Troll]: http://hjemmesider.diku.dk/~torbenm/Troll/

```php
<?php

class DndCharacter
{
    public function __construct()
    {
        throw new BadFunctionCallException("Please implement the DndCharacter class!");
    }
}
```

## 9/ Poker (3)

### Instructions
Pick the best hand(s) from a list of poker hands.

See wikipedia for an overview of poker hands.

```php
<?php

class Poker
{
    public array $bestHands = [];

    public function __construct(array $hands)
    {
        throw new BadFunctionCallException("Please implement the Poker class!");
    }
}
```

## 10/ Palindrome Products (3)

### Instructions
Detect palindrome products in a given range.

A palindromic number is a number that remains the same when its digits are
reversed. For example, `121` is a palindromic number but `112` is not.

Given a range of numbers, find the largest and smallest palindromes which
are products of two numbers within that range.

Your solution should return the largest and smallest palindromes, along with the
factors of each within the range. If the largest or smallest palindrome has more
than one pair of factors within the range, then return all the pairs.

## Example 1

Given the range `[1, 9]` (both inclusive)...

And given the list of all possible products within this range:
`[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18, 15, 21, 24, 27, 20, 28, 32, 36, 25, 30, 35, 40, 45, 42, 48, 54, 49, 56, 63, 64, 72, 81]`

The palindrome products are all single digit numbers (in this case):
`[1, 2, 3, 4, 5, 6, 7, 8, 9]`

The smallest palindrome product is `1`. Its factors are `(1, 1)`.
The largest palindrome product is `9`. Its factors are `(1, 9)` and `(3, 3)`.

## Example 2

Given the range `[10, 99]` (both inclusive)...

The smallest palindrome product is `121`. Its factors are `(11, 11)`.
The largest palindrome product is `9009`. Its factors are `(91, 99)`.

```php
<?php

function smallest(int $min, int $max): array
{
    throw new \BadFunctionCallException("Implement the smallest function");
}

function largest(int $min, int $max): array
{
    throw new \BadFunctionCallException("Implement the largest function");
}
```
